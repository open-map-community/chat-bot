const userHasRole = require('../../utils/userHasRole')
const addUserToRole = require('../../utils/addUserToRole')
const getUsersInRole = require('../../utils/getUsersInRole')
//const getMembersInRoom = require('../../utils/getMembersInRoom')
const getChannelByChannelID = require('../../utils/getChannelByChannelID')

const mapRoles = [
  'MAP',
  'Pedo',
  'Nepi',
  'Hebe',
  'CL',
  'BL',
  'GL',
  'LBL',
  'LGL',
  'Pedophile',
  'Nepiophile',
  'Hebephile',
  'Infantophile',
  'Korephile',
  'Korophile',
  'Child-Lover',
  'Girl-Lover',
  'Boy-Lover',
  'TGL',
  'TBL',
  'YAP',
]

async function devTemp4({ bot, message, context }) {
  const roomID = message.rid
  const roomToDo = context.argumentList[0]
  const roomToDoId = getChannelByChannelID(roomToDo)
  /*
  let result = await getMembersInRoom({
    roomName: roomToDo,
    count: 0,
  })
  */
  let result = await getUsersInRole({
    role: 'MAP',
    roomId: roomToDoId
  })
  let members = result.members

  await bot.sendToRoom(`Checking ${members.length} members in total and donning PPE... 🔍 🥼`, roomID)

  let progress = 0
  for (let member of members) {
    let assignNew = true
    progress += 1

    if (progress === 50) {
      progress = 0
      await bot.sendToRoom(`Still checking... 🧑‍🔬`, roomID)
    }

    // Check that user does not have MAP role
    /*if (userHasRole(member, 'MAP') || userHasRole(member, 'MAP w/o DMs')) {
      assignNew = true
      await bot.sendToRoom(
        `${member.name} has the MAP role.`,
        roomID,
      )*/

      for (let mapRole of mapRoles){
        if (userHasRole(member, mapRole)) {
          assignNew = false
          await bot.sendToRoom(
            member.name + ' has ' + mapRole,
            roomID,
          )
          break;
        }
      }
      if (assignNew){
        await addUserToRole(member, 'map')

        await bot.sendToRoom(
        member.name + ' has only the MAP role, so I\'m giving them the map role ',
        roomID,
      )
      }
    //}
  }

  await bot.sendToRoom(`Finished!`, roomID)
}


/*
async function getAllUsers(offset = 0) {
  let count = 100

  let result = await getUsersList({
    fields: {
      name: 1,
      username: 1,
    },
    query: {
      type: { $in: ['user'] },
    },
    count,
    offset,
  })

  let members = result.users

  if (members.length === count) {
    return [...members, ...(await getAllUsers(offset + count))]
  } else {
    return members
  }
}

async function devTemp4({bot, message}) {
  const roomID = message.rid

  await bot.sendToRoom(`Starting`, roomID)

  let members = await getAllUsers()
  
  await bot.sendToRoom(`Checking ${members.length} members in total....`, roomID)

  let progress = 0

  for (let member of members){
    let assignNew = false
    progress += 1

    // Give progress reports
    if (progress === 100) {
      progress = 0
      await bot.sendToRoom(`Am still checking....`, roomID)
    }

    // if user has MAP role
    if (await userHasRole(member, 'MAP') || await userHasRole(member, 'MAP w/o DMs')){
      assignNew = true
      // if user has any alternate MAP roles already
      for (let mapRole of mapRoles){
        if (await userHasRole(member, mapRole)) {
          assignNew = false
          break
        }
      }
      // assign new 'map' role
      if (assignNew) {
        await addUserToRole(member, 'map')
        await bot.sendToRoom(
          member.name + 'has the MAP role, so I\'m giving them the map role! :D',
          roomID,
        )
      }
    }
  }
  await bot.sendToRoom(
    'Finished! :D',
    roomID,
  )
}
*/
module.exports = {
  description: 'adding map role to people with MAP role',
  help: `${process.env.ROCKETCHAT_PREFIX} devTemp4 <room name>`,
  requireOneOfRoles: ['admin', 'Tech Admin', 'Community Admin'],
  call: devTemp4,
}
