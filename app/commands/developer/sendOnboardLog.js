const dayjs = require('dayjs')

async function sendOnboardLog({ bot, message, context, targetUser, commentField }) {

  const log_channelID = '45G7eM7o6TwNdFwNd'

  console.log(message)

  const object = {
    command: context.commandName,
    initiator: message.u.username,
    subject: targetUser.name,
    time: dayjs(message.ts.$date).toString(),
    ...commentField,
  }

  const message_to_send = `
${'```json'}
${JSON.stringify(object, undefined, 2)}
${'```'}
Subject username: @${targetUser.username}`

  await bot.sendToRoom(message_to_send, log_channelID)
}

module.exports = {
  description: 'Send a log event to the log channel.',
  help: `${process.env.ROCKETCHAT_PREFIX} log <message>`,
  requireOneOfRoles: ['admin'],
  call: sendOnboardLog,
}
