const roomInfo = require('./roomInfo')
const testCommand = require('./testCommand')
const testCooldown = require('./testCooldown')
const addRole = require('./addRole')
const removeRole = require('./removeRole')
const sendLog = require('./sendLog')
const testSendLog = require('./2sendLog')
const sanitise = require('./sanitise')

module.exports = {
  commands: {
    'Development Commands': {
      roomInfo,
      testCommand,
      testCooldown,
      addRole,
      removeRole,
      sendLog,
      testSendLog,
      sanitise,
    },
  },
}
