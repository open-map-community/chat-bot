const getUserByUsername = require('../../utils/getUserByUsername')

const getMembersInRoom = require('../../utils/getMembersInRoom')
const removeUserFromChannels = require('../../utils/removeUserFromChannels')
const inviteUserToChannel = require('../../utils/inviteUserToChannel')
const getUserInfo = require('../../utils/getUserInfo')
const getRooms = require('../../utils/getRooms')
const userHasRole = require('../../utils/userHasRole')

const courtyardID = 'YWRa5EiSttdypebrE'
const SHOULD_REMOVE = true

async function sanitise({ bot, message }) {
  const roomID = message.rid
  const user = await getUserByUsername(message.u.username)

  let result = await getMembersInRoom({
    roomName: 'onboarding',
    count: 0,
  })
  let members = result.members

  await bot.sendToRoom(`Checking ${members.length} members in total and donning PPE... 🔍 🥼`, roomID)

  let progress = 0
  for (let member of members) {
    progress += 1

    if (progress === 50) {
      progress = 0
      await bot.sendToRoom(`Still sanitising... 🧑‍🔬`, roomID)
    }

    const userInfo = await getUserInfo(member)
    const { statusConnection, roles, active, rooms } = userInfo

    //Check that user is not currently logged into OMC
    if (userInfo.statusConnection === 'offline') {

    // Check that user does not have MAP/Non-MAP/MAP w/o DMs roles
    if (!userHasRole(user, 'MAP') && !userHasRole(user, 'Non-MAP') && !userHasRole(user, 'MAP w/o DMs')) {

        // Don't kick the user from direct messages
        let roomsToKickFrom = userInfo.rooms.filter((room) => room.t !== 'd')

        // Format the list to work with the getRooms function
        roomsToKickFrom = await getRooms(
          roomsToKickFrom.map((room) => ({ roomId: room.rid })),
        )

        // Don't kick the user from default channels
        roomsToKickFrom = roomsToKickFrom.filter((room) => !room.default)

        await bot.sendToRoom(
          `${member.name} only has the user role, so I'm moving them to The Courtyard.`,
          roomID,
        )

        if (SHOULD_REMOVE) {
          let kicks = await removeUserFromChannels(member, roomsToKickFrom)
          kicks.forEach((kick) => {
            console.log('THIS IS A KICK OR AN ERROR FOR DEBUGGING:', kick)
          })

          await bot.sendToRoom(
            'Finished removing ' + member.name + ' from the onboarding channels! 🧪',
            roomID,
          )

          // Add the user to The Courtyard
          let invite = await inviteUserToChannel(member, {
            _id: courtyardID,
          })

          await bot.sendToRoom(
            'Added ' + member.name + ' to The Courtyard. 🌷🌹',
            roomID,
          )
        }
      }
    }
  }


  await bot.sendToRoom(`Finished!`, roomID)
}

module.exports = {
  description:
    "Remove all members without the map or nonmap roles from the onboarding channels. This is a workaround for `!map cleanup` . If working, use `!map cleanup 7` instead.",
  help: `${process.env.ROCKETCHAT_PREFIX} sanitise`,
  requireOneOfRoles: ['admin', 'Tech Admin', 'Community Admin'],
  call: sanitise,
}
