const dayjs = require('dayjs')

async function sendLog2({ bot, message, context, extraFields }) {
  const roomID = message.rid
  const log_channelID = 'qKvRFAGcAk8AhS8Hc'

  console.log(message)

  const message_to_send = {
    alias: message.u.username,
    attachments: [
      {
        collapsed: false,
        color: '#47bac0',
        ts: dayjs(message.ts.$date).toString(),
        fields: [
          { short: true, title: 'Command', value: context.commandName },
          { short: true, title: 'Initiator', value: message.u.username },
          {
            short: false,
            title: 'Time',
            value: dayjs(message.ts.$date).toString(),
          },
        ],
      },
    ],
  }

  await bot.sendToRoom(message_to_send, log_channelID)
}

module.exports = {
  description: 'Send a log event to the log channel.',
  help: `${process.env.ROCKETCHAT_PREFIX} log <message>`,
  requireOneOfRoles: ['admin'],
  call: sendLog2,
}
