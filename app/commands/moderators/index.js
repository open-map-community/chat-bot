const unMAP = require('./unMAP')
const offboard = require('./offboard')

module.exports = {
  commands: {
    'Moderation Commands': {
      unMAP,
      offboard,
    }
  },
}
