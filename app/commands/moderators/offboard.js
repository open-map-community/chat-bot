const getUserByUsername = require('../../utils/getUserByUsername')
const getChannelByChannelName = require('../../utils/getChannelByChannelName')
const getChannelsByChannelName = require('../../utils/getChannelsByChannelName')

const inviteUserToChannel = require('../../utils/inviteUserToChannel')
const removeUserFromChannels = require('../../utils/removeUserFromChannels')

const onboardingChannels = ['onboarding', 'introductions', 'troubleshooting']

async function offboard({ bot, message, context }) {
  const roomID = message.rid

  // Get the targetUser object
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)

  // Remind targetUser to be active
  if (context.argumentList.length > 1) {
    let sendMessage = context.argumentList[1]
    sendMessage = sendMessage.toLowerCase()
    if (sendMessage === 'send'){
        await bot.sendToRoom(
          'Inviting ' + targetUser.name + ' to The Dock ...',
          roomID,
        )
        let dock = await getChannelByChannelName('The-Dock')
        let invite = await inviteUserToChannel(targetUser, dock)

        const dockId = 'LoFyfCgSDb9yRuRDa'
        await bot.sendToRoom(
          'Oops! @' + targetUser.username + ', you haven\'t been talking enough in Onboarding! If you\'re ready to participate in our  community, go to #the-courtyard and ask staff to add you back!',
          dockId,
        )
    } else {
        await bot.sendToRoom(
        'Invalid command. Type `!map offboard` to add to Courtyard and remove from Onboarding channels, or `!map onboard send` to also send them to The Dock.',
        roomID,
      )
      return
    }
  }

  // Invite the targetUser to Courtyard
  await bot.sendToRoom(
    'Inviting ' + targetUser.name + ' to The Courtyard...',
    roomID,
  )
  let courtyard = await getChannelByChannelName('the-courtyard')
  let invite = await inviteUserToChannel(targetUser, courtyard)

  // Remove the targetUser from the onboarding channels
  await bot.sendToRoom(
    'Removing ' + targetUser.name + ' from The Onboarding channels 🧑‍⚖️🌲...',
    roomID,
  )
  let channels = await getChannelsByChannelName(onboardingChannels)
  let kicks = await removeUserFromChannels(targetUser, channels)
  kicks.forEach((kick) => {
    console.log('THIS IS A KICK OR AN ERROR FOR DEBUGGING:', kick)
  })

  await bot.sendToRoom(
    'Finished removing ' + targetUser.name + ' from the Onboarding channels! :D',
    roomID,
  )
    
}

module.exports = {
  description: 'Offboard a member.',
  help: `${process.env.ROCKETCHAT_PREFIX} offboard <username> <send(optional)>`,
  requireOneOfRoles: [
    'admin',
    'Tech Admin',
    'Community Admin',
    'Moderator',
  ],
  call: offboard,
}

