const levenshtein = require('js-levenshtein')

const definitions = {
  AAM: 'Adult-Attracted Minor. A minor who is attracted to adults.',
  ABDL: 'Adult Baby/Diaper Lover. An AB is an age player who desires to be treated like a baby, which may include dressing up in diapers or other baby-like clothes. A DL is someone who likes wearing and/or using diapers, but not necessarily with an age-play element.',
  'Allyn Walker': 'A former researcher for Old Dominion University who was driven out after public outrage at their pro-MAP book _Past the Dark Field_.',
  'Amos Yee': 'A Singaporean pro-c pedophile activist.',
  Anti: 'Someone hostile to paedophiles/MAPs.',
  'Anti-contact':
    'An ideology among some MAPs that sexual activity between minors and adults would still carry a fundamental risk for harm that outweighs potential benefits even if it were to be legalised and accepted in society.',
  AoA: 'Age of Attraction. The ages of people one finds sexually or romantically attractive, usually given as a range.',
  Autopaedophilia: 'Erotic pleasure from imagining oneself to be a child.',
  BoyChat:
    'A boy-love bulletin board for chat and discussion. https://boychat.org.',
  BL: 'Boy Lover. A boy-oriented MAP.',
  CBT: 'Cognitive-Behavioral Therapy. A popular psychotherapy technique based on identifying and reframing unhelpful thoughts/behaviours.',
  'Cheese Pizza':
    'A euphemism for child pornography. **C**heese **P**izza/**C**hild **P**ornography.',
  Chronophilia: 'An orientation of age-based attraction, such as pedophilia, teleiophilia, and gerontophilia',
  CL: 'Child Lover. A MAP who is attracted to children of more than one gender. Also used as a literal translation of the word paedophile.',
  COCSA: 'Child-On-Child Sexual Abuse',
  'Contact Neutral':
    'A MAP who is neither anti-contact or pro-contact. Some contact-neutral MAPs believe there is insufficient evidence in support of either side of the ideological spectrum.',
  CP: 'Child Pornography.',
  CSA: 'Child Sexual Abuse.',
  CSAM: 'Child Sexual Abuse Material. A term for child pornography that is used to emphasise a belief that all child pornography is abusive.',
  CSEM: 'Child Sexual Exploitation Material. A term for child pornography, child erotica, and fictional content that is used to emphasise a belief that this content is exploitative.',
  'David Thorstad': 'An American political activist and author who helped found NAMBLA and various LGBT groups.',
  'Dost test': 'A USA judicial precedent established in United States v. Dost to govern the legality of images of children.',
  DSM: 'The Diagnostic and Statistical Manual of Mental Disorders, by the American Psychiatric Association.',
  Ephebophilia:
    'An attraction to older adolescents (approximately ages 15-19).',
  Exclusive:
    'An attraction only to minors (ages 0-14) without any attraction to adults.',
  Fediverse: 'A decentralized Twitter-style platform with MAP instances like nnia.space, youjo.love, freak.university, lolison.top, and lolison.network.',
  FreeSpeechTube: 'A video-sharing website for pedophile activism, https://fstube.net.',
  Gerontophilia: 'An attraction to the elderly.',
  GirlChat:
    'A girl-love bulletin board for chat and discussion. https://annabelleigh.net.',
  GL: 'Girl Lover. A girl-oriented MAP.',
  Ghosting: 'Abandoning someone with zero further contact.',
  'Gibson v. Walden University': 'A USA court case in which Gary Gibson unsuccessfully fought Walden University for expelling him on the basis of his pedophilia.',
  'Harry Hay': 'An advocate for pedophile and LGBT rights, co-founder of NAMBLA and of the Mattachine Society and the Radical Faeries.',
  Hebephilia: 'An attraction to young adolescents (approximately ages 10-14)',
  ICD: 'International Classification of Diseases, by the World Health Organization.',
  IIOC: 'Illegal Images of Children. A term primarily in the UK.',
  ILGA: 'International Lesbian and Gay Association. Composed of several member organizations, including (formerly) pedophile organizations like NAMBLA and MARTHIJN till the United Nations pressured them to expel such groups. Now known as International Lesbian, Gay, Bisexual, Trans and Intersex Association.',
  Infantophilia: 'An attraction to babies',
  IPCE: 'International Pedophile and Child Emancipation. Used to hold meetings, now restricted to https://www.ipce.info/.',
  Korephile: 'A MAP who likes girls.',
  Korophile: 'A MAP who likes boys.',
  LBF: 'Little Boyfriend. A boy who a MAP has a close relationship/friendship with.',
  LBL: 'Little-Boy Lover. A MAP who is primarily attracted to boys under the age of approximately 6.',
  LGF: 'Little Girlfriend. A girl who a MAP has a close relationship/friendship with.',
  LGL: 'Little-Girl Lover. a MAP who is primarily attracted to girls under the age of approximately 6.',
  MAP: 'Minor-Attracted Person. MAPs are persons who are attracted to minors younger than 15 years old who are significantly younger than themselves. The term MAP is an umbrella term that includes nepiophilia, paedophilia, and hebephilia.',
  MARTHIJN: 'Martijn Association, a Dutch pedophile advocacy group, former member of ILGA, dissolved in 2014 by the Dutch Supreme Court for violating social norms.',
  'Marthijn Uittenbogaard': 'A Dutch pedophile activist who led PNVD and MARTHIJN.',
  Mastodon: 'The most popular user-interface for the Fediverse, often mistakenly used to refer to the Fediverse itself. nnia.space and freak.university use Mastodon, but youjo.love, lolison.top, and lolison.network use Pleroma.',
  Mesophilia: 'An attraction to middle-aged people.',
  MIP: 'Minor-Identifying Person. A person who identifies as a minor in some capacity. This may include transage persons, age players, littles, age regressors, ABDLs, autopaedophiles, and others.',
  MSC: 'MAP Support Club. A chat-based community for anti-contact MAPs (ages 13+) to receive support and find understanding among peers. https://mapsupport.club.',
  Muggle:
    'In the Harry Potter series of books, muggles are those who are incapable of doing magic. This term has been adapted by many communities to refer to those that do not belong to that community. In our case, a muggle is a non-MAP.',
  NAMBLA: 'North American Man-Boy Love Association. An advocacy organization for boys and Boy-Lovers. Former member of ILGA.',
  'Nelson Maatman': 'Pedophile advocate involved in PNVD and FreeSpeechTube, fled the Netherlands to seek asylum in Mexico, where he is now incarcerated.',
  Nepiophilia: 'An attraction to infants and toddlers.',
  Newgon: 'A MAP-advocacy organization, https://newgon.net.',
  NN: "Nonude. Clothed images of models created and distributed with intent to arouse the viewer by depicting the subject in 'revealing' clothing (eg, a thong), poses associated with sexual activity (eg, spreading legs apart) and/or camera angles that focus on the genital area. NN of minors is either illegal or a legal grey area in many countries, including Sweden.",
  NOMAP: 'Non-Offending Minor-Attracted Person.',
  Non: 'A non-MAP.',
  'Non-Exclusive': 'An attraction to both minors and adults.',
  OMC: 'Open MAP Community. A democracy-based community for MAPs (ages 15+) to give and receive peer support. OMC is the community you are currently in. https://mapcommunity.org.',
  Paedophilia:
    'An attraction to prepubescent minors (approximately ages 3-11). This term is widely used incorrectly to refer to minor attraction in general or illegal sexual activity with minors.',
  'Pedophilic Disorder':
    'A psychiatric disorder defined in the DSM-5 and ICD-11 as an adult paedophile who experiences clinically significant distress from their sexuality and/or engages in illegal sexual activity with a minor.',
  PIE: 'Pedophile Information Exchange. A defunct pedophile activist group.',
  PNVD: 'Party for Neighbourly Love, Freedom and Diversity, a Dutch political party for pedophile advocacy.',
  POCD: 'Paedophilic Obsessive-Compulsive Disorder (OCD). Reccurent, persistent thoughts of being a paedophile that are intrusive and cause clinically significant distress that may be accompanied by ritualistic behaviours that attempt to reduce distress but are ineffective.',
  PPD: 'Prevention Project Dunkelfeld. A German suport project for non-offending paedophiles. https://dont-offend.org.',
  'Pro-contact':
    'An ideology among some minor-attracted persons (MAPs) that consensual sexual activity between minors and adults would be fundamentally okay if there was no risk of harm from societal stigma or from legal ramifications.',
  SSRI: 'Selective Serotonin Reputake Inhibitor. A class of antidepressant medications commonly used to reduce sexual arousal.',
  SYF: 'Special Young Friend.',
  TBL: 'Teen-Boy Lover. A MAP who is attracted to boys between the ages of 13 and 19.',
  Teleiophilia: 'An attraction to adults/persons above the age of consent.',
  TGL: 'Teen-Girl Lover. A MAP who is attracted to girls between the ages of 13 and 19.',
  'Tom O\'Carroll': 'A British pedophile who was involved in PIE and IPCE, author of _Paedophilia: The Radical Case_ and _Michael Jackson\'s Dangerous Liaisons_.',
  VirPed:
    'Virtuous Pedophiles. A forum for paedophiles who are fundamentally against child-adult sex. https://virped.org,',
  VoA: 'Visions of Alice. A forum where GLs and others interested in girl love can share thoughts, opinions, feelings, and experiences. https://visionsofalice.net.',
  'Westermarck Effect':
    'Describes how siblings that grow up together from infancy generally do not develop an attraction to each other. While not originally applied to paedophilia, the term is sometimes used to describe how some paedophiles feel no attraction to their own children.',
  YF: 'Young Friend. A child who a MAP has a close relationship/friendship with.',
}

async function define({ bot, message, context }) {
  const roomID = message.rid
  let requestedDefinition = context.argumentList[0]
  // Get all the keys into a list
  definition_terms = Object.keys(definitions)

  console.log('\n\n\n\n', requestedDefinition)

  if (requestedDefinition === undefined || requestedDefinition === '') {
    let message = '#### The MAP Dictionary \n```xml\n'
    definition_terms.forEach((term) => {
      message = message + `|   ${term}\n`
    })
    message = message + '```'
    const sentMsg = await bot.sendToRoom(message, roomID)
  } else {
    // Calculate how similar the requested definition is to each definition key,
    // Using the Levenshtein algorithm.
    definition_keys_objects = definition_terms.map((term) => {
      return {
        term,
        distance: levenshtein(
          requestedDefinition.toLowerCase(),
          term.toLowerCase(),
        ),
      }
    })

    // Sort according to the distance
    definition_keys_objects = definition_keys_objects.sort(
      (a, b) => a.distance - b.distance,
    )

    for (let i = 0; i < 3; i++) {
      let term_object = definition_keys_objects[i]

      // Get the actual definition
      let definition = definitions[term_object.term]

      // Send the response
      const sentMsg = await bot.sendToRoom(
        `**${term_object.term}** — ${definition}`,
        roomID,
      )

      // Break the loop if the term is very similar to the requested term
      if (term_object.distance < 5) {
        break
      }
    }
  }
}

module.exports = {
  description: "OMC's dictionary of MAP relevant terms.",
  help: `${process.env.ROCKETCHAT_PREFIX} define <term (optional)>`,
  call: define,
}
