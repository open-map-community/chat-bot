const getUserByUsername = require('../../utils/getUserByUsername')

const addUserToRole = require('../../utils/addUserToRole')
const userHasRole = require('../../utils/userHasRole')

const mapRoles = [
  'MAP',
  'Pedo',
  'Nepi',
  'Hebe',
  'CL',
  'BL',
  'GL',
  'LBL',
  'LGL',
  'Pedophile',
  'Nepiophile',
  'Hebephile',
  'Infantophile',
  'Korephile',
  'Korophile',
  'Child-Lover',
  'Girl-Lover',
  'Boy-Lover',
  'TGL',
  'TBL',
  'YAP',
  'CAP',
]

async function newMAPRole({ bot, message, context }) {
  const roomID = message.rid
  const user = await getUserByUsername(message.u.username)
  console.log(user)

  // Get mapRole object 
  let mapRole = context.argumentList[0]

  // Check to see if the user entered a valid role
  if (mapRoles.includes(mapRole)) {

    // Check that the user doesn't already have that role
    if (!userHasRole(user, mapRole)){

        // Add the role to user
        await bot.sendToRoom(
           'Giving ' + user.name + ' the ' + mapRole + ' role...',
        roomID,
        )

        // Assign functionless, visible 'map' role, not permissions-bearing, invisible 'MAP' role
        if (mapRole === 'MAP') await (addUserToRole(user, mapRole.toLowerCase()))
        else await addUserToRole(user, mapRole)

        await bot.sendToRoom(
           'Finished giving ' + user.name + 'the ' + mapRole + ' role! :D',
           roomID,
        )
    }
    else {
        // User already has that role, no need to do anything else
        const response = 'You already have that role!'
        const sentMsg = await bot.sendToRoom(response, roomID)
        return
    }

  } else {
    // Send message to room saying it's an invalid role
    const response = mapRole + ' is an unavailable role. The currently available roles are: \'MAP\', \'Pedo\', \'Nepi\', \'Hebe\', \'CL\', \'BL\', \'GL\', \'LBL\', \'LGL\', \'Pedophile\', \'Nepiophile\', \'Hebephile\', \'Infantophile\', \'Korephile\', \'Korophile\', \'Child-Lover\', \'Girl-Lover\', \'Boy-Lover\', \'TGL\', \'TBL\', \'CAP\', and \'YAP\'. If you prefer a similar label and would like a role added, please let an administrator know. '
    const sentMsg = await bot.sendToRoom(response, roomID)
  }
}

module.exports = {
  description: 'Give yourself a map role.',
  help: `${process.env.ROCKETCHAT_PREFIX} newMAPRole <mapRole>. Available roles are: \`MAP\`, \`Pedo\`, \`Nepi\`, \`Hebe\`, \`CL\`, \`BL\`, \`GL\`, \`LBL\`, \`LGL\`, \`Pedophile\`, \`Nepiophile\`, \`Hebephile\`, \`Infantophile\`, \`Korephile\`, \`Korophile\`, \`Child-Lover\`, \`Girl-Lover\`, \`Boy-Lover\`, \`TGL\`, \`TBL\`, \`CAP\`, and \`YAP\`. If you prefer a similar label and would like a role added, please let an administrator know.`,
  requireOneOfRoles: [
    'MAP',
    'MAP w/o DMs',
    'Tech Admin',
    'Community Admin',
  ],
  call: newMAPRole,
}

