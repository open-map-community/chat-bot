const getUserByUsername = require('../../utils/getUserByUsername')

const addUserToRole = require('../../utils/addUserToRole')
const userHasRoles = require('../../utils/userHasRoles')
const removeUserFromRoles = require('../../utils/removeUserFromRoles')

const pronounRoles = [
  'he/him',
  'she/her',
  'she/it',
  'they/them',
  'he/they',
  'she/they',
  'they/she',
  'they/he',
  'xe/xem',
  'ae/aer',
  'it/its',
  'he/it',
  'it/he',
  'he/mew',
  'mew/he',
  'any pronouns'
]

async function addPronouns({ bot, message, context }) {
  const roomID = message.rid
  const user = await getUserByUsername(message.u.username)
  console.log(user)

  // Get pronounRole object 
  let pronounRole = context.argumentList[0]

  // Check to see if the user entered a valid pronoun role
  if (pronounRoles.includes(pronounRole)) {
    // Check if the user does already have a pronoun role
    if (user.roles.find(role => pronounRoles.includes(role))) {
      console.log("USER HAS ROLES \n\n\n")

      // Remove the user's current pronoun role
      await bot.sendToRoom(
        'Removing current pronoun roles from ' + user.name + ' ...',
        roomID,
      )
      console.log(user)

      await removeUserFromRoles(user, pronounRoles)
    }

    // Add the pronoun role to user
    await bot.sendToRoom(
      'Giving ' + user.name + ' the ' + pronounRole + ' role...',
      roomID,
    )
    await addUserToRole(user, pronounRole)

    await bot.sendToRoom(
      'Finished giving ' + user.name + 'the ' + pronounRole + ' role! :D',
      roomID,
    )

  } else {
    // Send message to room saying it's an invalid role
    const response = pronounRole + ' is an unavailable pronoun role. The currently available roles are: `he/him`, `she/her`,`she/it`, `they/them`; `he/they`, `she/they`, `they/she`, `they/he`; `xe/xem`, `ae/aer`, `it/its`, `it/he`, `he/it`, `he/mew`, `mew/he`; and `any pronouns`. If you use other pronouns and would like a role added for them, please let an administrator know. '
    const sentMsg = await bot.sendToRoom(response, roomID)
  }
}

module.exports = {
  description: 'Give yourself a pronoun role.',
  help: `${process.env.ROCKETCHAT_PREFIX} addPronouns <your pronouns>. Available pronouns are: \`he/him\`, \`she/her\`, \`she/it\`, \`they/them\`; \`he/they\`, \`she/they\`, \`they/she\`, \`they/he\`; \`xe/xem\`, \`ae/aer\`, \`it/its\`, \`it/he\`, \`he/it\`, \`he/mew\`, \`mew/he\`; and \`any pronouns\`. If you use other pronouns and would like a role added for them, please let an administrator know.`,
  call: addPronouns,
}

