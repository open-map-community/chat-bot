const getUsersInRole = require('../../utils/getUsersInRole')

const STAFF = [
  'staff',
  'voter',
  'voters',
]

const MOD = [
  'moderator',
  'moderators',
  'mod',
  'mods',
  'global moderator',
  'ass mod',
  'associate mod',
  'associate moderator',
]
const ADMIN = [
  'admin',
  'admins',
  'administrator',
  'administrators',
  'tech',
  'tech admin',
  'financial admin',
]

async function mention({ bot, message, context }) {
  const roomID = message.rid

  // Get the arguments
  let targetRole = context.argumentList[0]

  // Set up variables
  let users = []
  let messageToSend = ''

  // Was a role specified?
  if (targetRole !== undefined) {
    // Start checking which role was specified, then get users in that role

    if (STAFF.includes(targetRole.toLowerCase())) {
      messageToSend = `Mentioning all staff: `
      users = [
        ...(await getUsersInRole('Moderator')).users,
        ...(await getUsersInRole('Associate Moderator')).users,
        ...(await getUsersInRole('Tech Admin')).users,
        ...(await getUsersInRole('Financial Admin')).users,
        ...(await getUsersInRole('Community Admin')).users,
      ]
    } else if (MOD.includes(targetRole.toLowerCase())) {
      messageToSend = `Mentioning all moderators: `
      users = [
        ...(await getUsersInRole('Moderator')).users,
        ...(await getUsersInRole('Associate Moderator')).users,
      ]
    } else if (ADMIN.includes(targetRole.toLowerCase())) {
      messageToSend = `Mentioning all administrators: `
      users = [
        ...(await getUsersInRole('Tech Admin')).users,
        ...(await getUsersInRole('Financial Admin')).users,
        ...(await getUsersInRole('Community Admin')).users,
      ]
    } else {
      // If no valid role, tell the user
      await bot.sendToRoom(
        'You can only mention admins, mods, or helpers with this command.',
        roomID,
      )
      return
    }
  } else {
    messageToSend = `Mentioning all staff and helpers: `
    // No role specified, mention all staff members
    users = [...users, ...(await getUsersInRole('Helper')).users]
    users = [...users, ...(await getUsersInRole('Moderator')).users]
    users = [...users, ...(await getUsersInRole('Associate Moderator')).users]
    users = [...users, ...(await getUsersInRole('Tech Admin')).users]
    users = [...users, ...(await getUsersInRole('Financial Admin')).users]
    users = [...users, ...(await getUsersInRole('Community Admin')).users]
  }

  // Prepare message
  for (let user of users) {
    messageToSend = `${messageToSend} @${user.username} `
  }

  // Send the message
  await bot.sendToRoom(messageToSend, roomID)
}

module.exports = {
  description: 'Mention all staff members.',
  help: `${process.env.ROCKETCHAT_PREFIX} mention <optional admin|mod|voter>`,
  call: mention,
  cooldown: 60 * 30,
}
