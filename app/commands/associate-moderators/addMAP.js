const sendLog = require('../developer/sendLog')

const getUserByUsername = require('../../utils/getUserByUsername')
const getChannelsByChannelName = require('../../utils/getChannelsByChannelName')

const addUserToRole = require('../../utils/addUserToRole')
const userHasRole = require('../../utils/userHasRole')

const inviteUserToChannels = require('../../utils/inviteUserToChannels')

const defaultChannels = [
  'Arts-and-Music',
  'gaming',
  'life-experiences',
  'MAP-Talk',
  'miscellaneous',
  'miscellaneous-polls',
  'serious',
  'support-requests-1',
  'support-requests-2',
  'venting',
  'food',
  'general',
  'media',
  'news',
  'proposals',
  'proposals-discussions',
  'proposals-polls',
  'spam',
  'stem',
]

const mapRoles = [
  'MAP',
  'Pedo',
  'Nepi',
  'Hebe',
  'CL',
  'BL',
  'GL',
  'LBL',
  'LGL',
  'Pedophile',
  'Nepiophile',
  'Hebephile',
  'Infantophile',
  'Korephile',
  'Korophile',
  'Child-Lover',
  'Girl-Lover',
  'Boy-Lover',
  'TGL',
  'TBL',
  'YAP',
  'CAP',
]

async function addMAP({ bot, message, context }) {
  const roomID = message.rid
  const user = await getUserByUsername(message.u.username)

  // Get the targetUser object
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)

  // Check if the targetUser does not already have the MAP role
  if (!userHasRole(targetUser, 'MAP')) {

    // Check for specified Map role
    if (context.argumentList.length > 1) {

      // Get mapRole object 
      let mapRole = context.argumentList[1]

      // If the user entered a valid role, assign it
      if (mapRoles.includes(mapRole)) {

        await bot.sendToRoom(
          'Assigning the ' + mapRole + ' role to ' + targetUser.name + "...",
          roomID,
        )
        await addUserToRole(targetUser, mapRole)
      } else {
        await bot.sendToRoom(
          'Invalid role. Please choose from: `MAP`, `Pedo`, `Nepi`, `Hebe`, `CL`, `BL`, `GL`, `LBL`, `LGL`, `Pedophile`, `Nepiophile`, `Hebephile`, `Infantophile`, `Korephile`, `Korophile`, `Child-Lover`, `Girl-Lover`, and `Boy-Lover`.',
          roomID,
        )
        return
      }
    } else {
      await addUserToRole(targetUser, 'map')
    }


    // Add the MAP role to targetUser
    await bot.sendToRoom(
      'Giving ' + targetUser.name + ' the MAP role...',
      roomID,
    )
    await addUserToRole(targetUser, 'MAP')

    // Invite the targetUser to default channels
    await bot.sendToRoom(
      'Inviting ' + targetUser.name + ' to the default channels...',
      roomID,
    )
    let channels = await getChannelsByChannelName(defaultChannels)
    let invites = await inviteUserToChannels(targetUser, channels)
    invites.forEach((invite) => {
      console.log('THIS IS A INVITE OR AN ERROR FOR DEBUGGING:', invite)
    })

    // Initiate new direct message with MAPped member
    let message_to_send =
      '***Congrats on earning the MAP role! If you would like, you may send anonymous feedback to staff using the !map feedback command here in this DM room (requires E2E encryption in this DM room to be disabled). Also, you may now join these opt-in channels if you would like to: #activism #contact-ideology #Cute-boys #Cute-girls #cw-general #development #littles #map-talk-topics #paraphilias #spirituality #the-courtyard #The-Dock #staff #staff-handbook #staff-moderation #staff-polls :)*** \n * '
    message_to_send = `${message_to_send} \n`
    await bot.sendDirectToUser(message_to_send, targetUser.username)
    await bot.sendToRoom(
      'Finished inviting ' + targetUser.name + ' to the default channels! :D',
      roomID,
    )

    // Send log to #Private-Onboarding-log
    await sendLog.call({
      bot,
      message,
      context,
      extraFields: { subject: targetUser.name },
    })

  } else {
    // User already has the MAP role, no need to do anything else
    const response = targetUser.name + ' already has the MAP role'
    const sentMsg = await bot.sendToRoom(response, roomID)
    return
  }
}

module.exports = {
  description:
    'Give a member the MAP role and invite them to the appropriate channels.',
  help: `${process.env.ROCKETCHAT_PREFIX} addMAP <username> "Map role"`,
  requireOneOfRoles: [
    'admin',
    'Tech Admin',
    'Community Admin',
    'Moderator',
    'Associate Moderator',
  ],
  call: addMAP,
}
