const sendLog = require('../developer/sendLog')

const getUserByUsername = require('../../utils/getUserByUsername')

const addUserToRole = require('../../utils/addUserToRole')
const userHasRole = require('../../utils/userHasRole')

async function addDM({ bot, message, context }) {
  const roomID = message.rid
  const user = await getUserByUsername(message.u.username)

  // Get the targetUser object
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)

  // Check if the targetUser does not already have the DM role
  if (!userHasRole(targetUser, 'DM')) {
    // Add the MAP role to targetUser
    await bot.sendToRoom(
      'Giving ' + targetUser.name + ' the DM role...',
      roomID,
    )
    await addUserToRole(targetUser, 'DM')

    await bot.sendToRoom(
      'Finished giving ' + targetUser.name + ' the DM role! :D',
      roomID,
    )

    // Send log
    await sendLog.call({
      bot,
      message,
      context,
      extraFields: { subject: targetUser.name },
    })
  } else {
    // User already has the DM role, no need to do anything else
    const response = targetUser.name + ' already has the DM role'
    const sentMsg = await bot.sendToRoom(response, roomID)
    return
  }
}

module.exports = {
  description: 'Give a member the DM role.',
  help: `${process.env.ROCKETCHAT_PREFIX} addDM <username>`,
  requireOneOfRoles: [
    'admin',
    'Tech Admin',
    'Community Admin',
    'Moderator',
    'Associate Moderator',
  ],
  call: addDM,
}
