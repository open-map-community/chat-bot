const addDM = require('./addDM')
const addMAP = require('./addMAP')
const addNonMAP = require('./addNonMAP')
const cleanup = require('./cleanup')
const log = require('./log')
const onboard = require('./onboard')
const userInfo = require('./userInfo')

module.exports = {
  commands: {
    'Associate Moderation Commands': {
      addDM,
      addMAP,
      addNonMAP,
      cleanup,
      log,
      onboard,
      userInfo
    },
  },
}
