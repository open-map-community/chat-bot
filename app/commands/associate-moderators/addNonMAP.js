const sendLog = require('../developer/sendLog')

const getUserByUsername = require('../../utils/getUserByUsername')
const getChannelsByChannelName = require('../../utils/getChannelsByChannelName')

const addUserToRole = require('../../utils/addUserToRole')
const userHasRole = require('../../utils/userHasRole')

const inviteUserToChannels = require('../../utils/inviteUserToChannels')

const defaultChannels = [
  'Arts-and-Music',
  'gaming',
  'MAP-Talk',
  'miscellaneous',
  'miscellaneous-polls',
  'serious',
  'support-requests-2',
  'food',
  'general',
  'media',
  'news',
  'proposals',
  'proposals-discussions',
  'proposals-polls',
  'spam',
  'stem',
]

async function addNonMAP({ bot, message, context }) {
  const roomID = message.rid
  const user = await getUserByUsername(message.u.username)

  // Get the targetUser object
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)

  // Check if the targetUser does not already have the Non-MAP role
  if (!userHasRole(targetUser, 'Non-MAP')) {
    // Add the Non-MAP role to targetUser
    await bot.sendToRoom(
      'Giving ' + targetUser.name + ' the Non-MAP role...',
      roomID,
    )
    await addUserToRole(targetUser, 'Non-MAP')

    // Invite the targetUser to default channels
    let channels = await getChannelsByChannelName(defaultChannels)
    await bot.sendToRoom(
      'Inviting ' + targetUser.name + ' to the default Non-MAP channels...',
      roomID,
    )
    let invites = await inviteUserToChannels(targetUser, channels)
        // Initiate new direct message with MAPped member
    let message_to_send =
      '***Congrats on earning the Non-MAP role! If you would like, you may send anonymous feedback to staff using the !map feedback command here in this DM room (requires E2E encryption in this DM room to be disabled). Also, you may now join these opt-in channels if you would like to: #activism #development #littles #map-talk-topics #paraphilias #spirituality #the-courtyard #The-Dock #staff #staff-handbook #staff-moderation #staff-polls . You may also join any other channels in the directory (the globe icon in the sidebar) :)*** \n * '
    message_to_send = `${message_to_send} \n`
    await bot.sendDirectToUser(message_to_send, targetUser.username)
 

    await bot.sendToRoom(
      'Finished inviting ' +
        targetUser.name +
        ' to the default Non-MAP channels! :D',
      roomID,
    )

    // Send log
    await sendLog.call({
      bot,
      message,
      context,
      extraFields: { subject: targetUser.name },
    })
  } else {
    // User already has the MAP role, no need to do anything else
    const response = targetUser.name + ' already has the Non-MAP role'
    const sentMsg = await bot.sendToRoom(response, roomID)
    return
  }
}

module.exports = {
  description:
    'Give a non-map the Non-MAP role and invite them to the appropriate channels.',
  help: `${process.env.ROCKETCHAT_PREFIX} addNon-MAP <username>`,
  requireOneOfRoles: [
    'admin',
    'Tech Admin',
    'Community Admin',
    'Moderator',
    'Associate Moderator',
  ],
  call: addNonMAP,
}
