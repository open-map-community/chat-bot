const sendOnboardLog = require('../developer/sendOnboardLog')

const getUserByUsername = require('../../utils/getUserByUsername')
const getChannelsByChannelName = require('../../utils/getChannelsByChannelName')

const inviteUserToChannels = require('../../utils/inviteUserToChannels')
const removeUserFromChannel = require('../../utils/removeUserFromChannel')

const defaultChannels = ['onboarding', 'introductions', 'troubleshooting']

async function onboard({ bot, message, context }) {

  // Check for comments
  if(context.argumentList.length > 2) {
    await bot.sendToRoom(
    'Please put entire comment in quotation marks, for example: !map onboard \"nonmap, please verify as user123 on VoA\"',
    roomID,
    )
    return
  }

  const roomID = message.rid
  const user = await getUserByUsername(message.u.username)

  // Get the targetUser object
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)

  // Invite the targetUser to default channels
  await bot.sendToRoom(
    'Inviting ' + targetUser.name + ' to the channels...',
    roomID,
  )
  let channels = await getChannelsByChannelName(defaultChannels)
  let invites = await inviteUserToChannels(targetUser, channels)
  invites.forEach((invite) => {
    console.log('THIS IS A INVITE OR AN ERROR FOR DEBUGGING:', invite)
  })

  // Remove the targetUser from the channel
  await bot.sendToRoom(
    'Removing ' + targetUser.name + ' from The Courtyard 🧑‍⚖️🌲...',
    roomID,
  )
  
  let kicks = await removeUserFromChannel(
    targetUser, { _id: roomID }
    )

  await bot.sendToRoom(
    'Finished inviting ' + targetUser.name + ' to the default channels! :D',
    roomID,
  )

  // Get comments for log
  let comment = 'Edit this line to add comments'
  if (context.argumentList.length > 1) {
    comment = context.argumentList[1]
  }

  // Send Onboarding log
  await sendOnboardLog.call({
    bot,
    message,
    context,
    targetUser,
    commentField: { comments: comment }
  })

  // Add to Onboard List
  await bot.sendToRoom('@' + targetUser.username, "5uPMyo7RABSzQCH7T")
}

module.exports = {
  description: 'Onboard a member',
  help: `${process.env.ROCKETCHAT_PREFIX} onboard <username> "optional comments"`,
  requireOneOfRoles: [
    'admin',
    'Tech Admin',
    'Community Admin',
    'Moderator',
    'Associate Moderator',
    'Helper'
  ],
  call: onboard,
}
