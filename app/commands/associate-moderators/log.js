const getUserByUsername = require('../../utils/getUserByUsername')
const deleteMessage = require('../../utils/deleteMessage')

async function log({ bot, message, context }) {
    const roomID = message.rid

  // Get the log message by removing "PREFIX COMMAND" from the message
  const log_message = message.msg.substring(
    `${context.prefix} ${context.commandName} `.length,
  )

  //Check if room type is private
  if (message.roomType === 'p') {

// Send the log to room
await bot.sendToRoom(log_message, roomID)

 // Delete the user's message
 await deleteMessage(roomID, message._id)
  } else {

    //send message to room explaining that this command only works in private channels
    await bot.sendToRoom(
      'This command is intended for creating logs and must be called in a private channel such as #private-moderation-log', 
      roomID,
    )

    let message_to_DM = 
    '_**This is the log you attempted to send outside of a private channel. Please send this log in a private channel instead.**_ \n \n'
    message_to_DM = `${message_to_DM} ${log_message} \n`
    await bot.sendDirectToUser(message_to_DM, message.u.username)
    
    // Delete the user's message
      await deleteMessage(roomID, message._id)
  }
}

module.exports = {
  description:
    "Repeats log text after command.",
  help: `${process.env.ROCKETCHAT_PREFIX} log <message>`,
  requireOneOfRoles: [
    'admin',
    'Tech Admin',
    'Community Admin',
    'Financial Admin',
    'Moderator',
    'Associate Moderator',
  ],
  call: log,
}
