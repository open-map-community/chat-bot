const { api } = require('@rocket.chat/sdk')

const getChannelByChannelName = require('./getChannelByChannelName')

const getChannelsByChannelName = async channelNames => {
  let channels = []
  for (let channelName of channelNames) {
    channels.push(await getChannelByChannelName(channelName))
  }
  return channels
}

module.exports = getChannelsByChannelName
