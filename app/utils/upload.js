const { api } = require('@rocket.chat/sdk')

const upload = async (roomId, options = undefined) => {
  let result = await api.get(`rooms.upload`, { roomId, ...options })

  console.log(result)
  if (result.success) {
    return result
  } else {
    console.error(result)
  }
}

module.exports = upload
